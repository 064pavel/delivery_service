<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function register(array $data)
    {    
          $user = User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
          ]);
    
          if($user){
            $response = $this->generateTokenResponse($user);
          } else $response = ['message' => 'Registration error'];

          return $response;
    }

    public function login(array $data)
    {
      $user = User::where('email', $data['email'])->first();
    
      if(!$user || !Hash::check($data['password'], $user->password)){
          return response()->json(['message' => 'Bad creds'], 401);
      }

      $response = $this->generateTokenResponse($user);

      return $response;
    }

    private function generateTokenResponse(User $user): array
    {
        $token = $user->createToken('devileryServiceToken')->plainTextToken;

        return [
          'user' => $user,
          'token' => $token
        ];
  
    }
}