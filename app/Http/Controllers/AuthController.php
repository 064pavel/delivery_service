<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\AuthService;
use Dotenv\Exception\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;

class AuthController extends Controller
{

    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(RegisterRequest $request): Response
    {
        try{

            $response = $this->authService->register($request->validated());

            return response($response, 201);

        } catch(ValidationException $e){
            return response(['error' => 'Validation failed', 'message' => $e->getMessage()], 422);
        } catch(QueryException $e){
            return response(['error' => 'Database failed', 'message' => $e->getMessage()], 500);
        } catch(\Exception $e){
            return response(['error' => 'Unexpected error', 'message' => $e->getMessage()], 500);
        }

    }

    public function login(LoginRequest $request): Response
    {
        try{
    
            $response = $this->authService->login($request->validated());
            
            return response($response, 200);

        }catch(ValidationException $e){
            return response(['error' => 'Validation failed', 'message' => $e->getMessage()], 422);
        }catch(QueryException $e){
            return response(['error' => 'Database failed', 'message' => $e->getMessage()], 500);
        }catch(\Exception $e){
            return response(['error' => 'Unexpected error', 'message' => $e->getMessage()], 500);
        }
    } 

    public function logout(): Response
    {
        try{
            auth()->user()->tokens()->delete();

            return response(['message' => 'Logged out']);
        } catch(\Exception $e){
            return response(['error' => 'Unexpected error', 'message' => $e->getMessage()], 500);
        }
    }
}
