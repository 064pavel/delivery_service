<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Delivery;
use App\Models\Item;
use App\Models\Order;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $roles = [
            ["id" => 1, "name" => "customer"],
            ["id" => 2, "name" => "seller"],
            ["id" => 3, "name" => "courier"], 
        ];

        Role::insert($roles);

        $userTest = [
            [
                "id" => 1,
                "name" => "seller test",
                'phone' => fake()->unique()->e164PhoneNumber(),
                'email' => fake()->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'role_id' => 2,
                'remember_token' => Str::random(10),
            ],
            [
                "id" => 2,
                "name" => "courier test",
                'phone' => fake()->unique()->e164PhoneNumber(),
                'email' => fake()->unique()->safeEmail(),
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'role_id' => 3,
                'remember_token' => Str::random(10),
            ],
        ];

        User::insert($userTest);

        User::factory(10)->create();

        $statuses = [
            ['id' => 1, 'name' => 'pending'],
            ['id' => 2, 'name' => 'processing'],
            ['id' => 3, 'name' => 'shipped'],
            ['id' => 4, 'name' => 'delivered'],
            ['id' => 5, 'name' => 'cancelled']
        ];

        Status::insert($statuses);

        Order::factory(6)->create();

    }
}
