<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'customer_id' => User::query()->where('role_id', 1)->inRandomOrder()->value('id'),
            'courier_id' => User::query()->where('role_id', 3)->inRandomOrder()->value('id'),
            'price' => fake()->numberBetween(399, 29999),
            'adress' => '3136 Broad Street',
            'delivery_date' => Carbon::tomorrow(),
            'status_id' => Status::query()->inRandomOrder()->value('id'),
        ];
    }
}
